from django.contrib import admin

# Register your models here.
from schedule.models import Schedule


@admin.register(Schedule)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ['operating_agency', 'person', 'start_date', 'ending_date', 'payment', 'cantNationals',
                    'cantForeigns', ]
    list_display_links = ['operating_agency', 'person', 'start_date', 'ending_date', 'payment', 'cantNationals',
                          'cantForeigns', ]
    raw_id_fields = ('operating_agency', 'person', )
    search_fields = ('user__first_name', )
