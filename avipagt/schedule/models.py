from django.db import models

# Create your models here.
from operatingAgency.models import OperatingAgency
from person.models import Person


class Schedule(models.Model):
    operating_agency = models.ForeignKey(OperatingAgency, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    start_date = models.DateField()
    ending_date = models.DateField()
    payment = models.BooleanField(default=False)
    payment_dispo = models.BooleanField(default=False)
    cantNationals = models.IntegerField(blank=True, null=True)
    cantForeigns = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'schedule'
        verbose_name_plural = 'shedules'

    def __bool__(self):
        return self.payment
