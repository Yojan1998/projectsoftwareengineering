from django.apps import AppConfig


class OperatingagencyConfig(AppConfig):
    name = 'operatingAgency'
