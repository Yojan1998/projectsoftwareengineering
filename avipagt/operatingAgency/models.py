from django.db import models


# Create your models here.
class OperatingAgency(models.Model):
    business_name = models.CharField(max_length=100)
    legal_representative = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'operatingAgency'
        verbose_name_plural = 'operatingAgencys'

    def __str__(self):
        return self.business_name
