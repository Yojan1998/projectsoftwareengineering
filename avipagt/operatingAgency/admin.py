from django.contrib import admin

# Register your models here.
from operatingAgency.models import OperatingAgency


@admin.register(OperatingAgency)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ['business_name', 'legal_representative', ]
    list_display_links = ['business_name', 'legal_representative', ]
    search_fields = ('business_name',)
