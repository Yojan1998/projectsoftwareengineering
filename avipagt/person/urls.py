from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from person.views import AsignarListView, AsignarACreate, ActualizarAUpdate, \
    RegistrarGuiaCreate, IndexGuiaView, PagosGuiaView, AgendaUpdate, ListPersonsView

urlpatterns = [
    path('registrar/', RegistrarGuiaCreate.as_view(), name="registrar-create"),
    path('actualizarA/<int:pk>/', ActualizarAUpdate.as_view(), name="actualizarA-update"),
    path('actualizar-agenda/<int:pk>/', AgendaUpdate.as_view(), name="actualizar-agenda"),
    path('asignarA/', AsignarACreate.as_view(), name="asignarA-create"),
    path('asignar/', AsignarListView.as_view(), name="asignar-list"),
    path('persons/', ListPersonsView.as_view(), name="persons-list"),
    path('index-guia/', IndexGuiaView.as_view(), name="index_guia"),
    path('pagos-guia/', PagosGuiaView.as_view(), name="pagos_guia"),
    path('', LoginView.as_view(redirect_authenticated_user=True), name='login'),
    path('logout/', LogoutView.as_view(next_page="/"), name='logout'),
]
