from django.contrib import admin

# Register your models here.
from person.models import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ['user', '__str__', 'identification_card', 'phone', 'adress',
                    'date_of_birth', 'rol', 'professional_card', ]
    list_display_links = ['user', '__str__', 'identification_card', 'phone', 'adress',
                          'date_of_birth', 'rol', 'professional_card', ]
    raw_id_fields = ('user',)
    search_fields = ('user__first_name',)
