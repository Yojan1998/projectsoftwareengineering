from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class Person(models.Model):
    SECRETARY = 'secretary'
    TOUR_GUIDE = 'tour guide'
    ROL = ((TOUR_GUIDE, TOUR_GUIDE), (SECRETARY, SECRETARY))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    identification_card = models.PositiveIntegerField()
    phone = models.PositiveIntegerField()
    adress = models.CharField(max_length=45)
    date_of_birth = models.DateField()
    rol = models.CharField(choices=ROL, max_length=45)
    professional_card = models.CharField(max_length=4)

    class Meta:
        verbose_name = 'person'
        verbose_name_plural = 'persons'

    def __str__(self):
        return self.user.first_name








