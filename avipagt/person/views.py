from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

# Create your views here.
from django.shortcuts import redirect
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import UpdateView, CreateView

from person.models import Person
from schedule.models import Schedule


class IndexGuiaView(ListView):
    template_name = 'usuarioGuia/index.html'
    model = Schedule

    def get_queryset(self):
        queryset = super(IndexGuiaView, self).get_queryset()
        # Get the q GET parameter
        queryset = queryset.filter(person__user=self.request.user)
        q = self.request.GET.get('q')
        # q = q.replace(" ", "+")
        if q is None:
        # Return the base queryset
            return queryset
        # Return a filtered queryset
        return queryset

class PagosGuiaView(ListView):
    template_name = 'usuarioGuia/Pagos.html'
    model = Schedule

    def get_queryset(self):
        queryset = super(PagosGuiaView, self).get_queryset()
        # Get the q GET parameter
        queryset = queryset.filter(person__user=self.request.user,payment=True)
        q = self.request.GET.get('q')
        # q = q.replace(" ", "+")
        if q is None:
        # Return the base queryset
            return queryset
        # Return a filtered queryset
        return queryset


class AsignarListView(ListView):
    template_name = 'usuarioSecretaria/Asignar.html'
    model = Schedule
    queryset = Schedule.objects.all()

    def dispatch(self, request, *args, **kwargs):
        # check if there is some video onsite
        if not Person.objects.filter(rol="secretary",user=request.user).exists():
            return redirect('/index-guia')
        else:
            return super(AsignarListView, self).dispatch(request, *args, **kwargs)


class AsignarACreate(CreateView):
    template_name = 'usuarioSecretaria/AsignarA.html'
    model = Schedule
    fields = ['person', 'operating_agency', 'start_date', 'ending_date', 'cantNationals',
              'cantForeigns', ]
    success_url = "/asignar"


class ListPersonsView(ListView):
    template_name = 'usuarioSecretaria/Actualizar.html'
    model = Person
    # rol = Person.objects.filter(rol='tour guide')
    queryset = Person.objects.all()

class AgendaUpdate(UpdateView):
    template_name = 'usuarioSecretaria/ActualizarA.html'
    model = Schedule
    fields = ['payment', 'payment_dispo',]
    success_url = "/asignar/"
    def get_context_data(self, **kwargs):
        data = super(AgendaUpdate, self).get_context_data()
        data["sin_name"] = True
        return data

class ActualizarAUpdate(UpdateView):
    template_name = 'usuarioSecretaria/ActualizarA.html'
    model = Person
    fields = ['phone', 'adress', 'date_of_birth', 'professional_card']
    success_url = "/persons/"

    def get_context_data(self, **kwargs):
        data = super(ActualizarAUpdate, self).get_context_data()
        person = Person.objects.get(id=self.kwargs.get("pk"))
        user = User.objects.get(person=person)
        data["nombre"] = user.first_name
        data["apellido"] = user.last_name
        return data

    def form_valid(self, form):
        print(self.kwargs.get("pk"))
        person = Person.objects.get(id=self.kwargs.get("pk"))
        user = User.objects.get(person=person)
        user.first_name = self.request.POST.get('first_name')
        user.last_name = self.request.POST.get('last_name')
        user.save()
        # form.instance.user = user
        return super(ActualizarAUpdate, self).form_valid(form)


class RegistrarGuiaCreate(CreateView):
    template_name = 'usuarioSecretaria/Registrar.html'
    model = Person
    fields = ['identification_card', 'phone', 'adress', 'date_of_birth',
              'professional_card']
    success_url = "/persons/"

    def form_valid(self, form):
        # print(dir(form))
        print(self.request.POST.get('first_name'))
        user = User(username=form.instance.identification_card,first_name=self.request.POST.get('first_name'),
                    last_name=self.request.POST.get('last_name'))
        user.save()
        form.instance.user = user
        return super(RegistrarGuiaCreate, self).form_valid(form)